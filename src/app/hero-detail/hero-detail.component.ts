import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../hero';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';

import { HeroService } from '../hero.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnInit {

  @Input() hero?: Hero;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private heroService: HeroService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getHero();
  }
  goBack(): void {
    this.location.back();
  }

  getHero(): void {
    const id = Number(this.route.snapshot.paramMap.get('id'));
    this.heroService.getHero(id)
      .subscribe(hero => this.hero = hero);
    if (!this.hero) {
      console.info(this.hero)
      //this.location.go('dashboard')
      //this.router.navigate(['/hello'])
      //this.route.navigateByUrl("/team/33/user/11");
      //this.router.navigate(['aa'], { queryParams: {id: 37, username: 'jimmy'} })
      // this.router.navigate(['aa'], { relativeTo: this.route })
      this.router.navigate(['aaa'], { skipLocationChange: false  })
    }
  }

}
